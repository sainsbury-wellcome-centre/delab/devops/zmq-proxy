import zmq
import typer
import datetime as dt


def main(topic: str, addr:str = 'localhost',port: int = 7001):

    ctx = zmq.Context().instance()

    sub = ctx.socket(zmq.SUB)
    sub.setsockopt(zmq.HEARTBEAT_IVL, 60000)
    sub.setsockopt_string(zmq.SUBSCRIBE, f'{topic}')
    sub.connect(f"tcp://{addr}:{port}")

    while True:
        msg = sub.recv_multipart()
        print(f"{dt.datetime.now().isoformat()} - {msg}")
        

if __name__ == '__main__':
    typer.run(main)
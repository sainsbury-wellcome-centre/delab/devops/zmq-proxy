# Espresso Pattern
# This shows how to capture data using a pub-sub proxy
#
import typer
from threading import Thread
import zmq
import datetime as dt
import time
# listener thread
# The listener receives all messages flowing through the proxy, on its
# pipe. Here, the pipe is a pair of ZMQ_PAIR sockets that connects
# attached child threads via inproc. In other languages your mileage may vary:


def heartbeat(pushport):
    # Set up subscriber to all messages
    ctx = zmq.Context()
    heart_socket = ctx.socket(zmq.PUSH)
    # # Since this heart_socket is just here to make sure we get all messages,
    # # we set CONFLATE to 1, to not use up memory
    heart_socket.setsockopt(zmq.LINGER, 0)
    heart_socket.connect(f"tcp://localhost:{pushport}")


    # Print everything that arrives on pipe
    time.sleep(1)
    while True:
        try:
            heart_socket.send(b'heartbeat')
            time.sleep(60)
        except zmq.ZMQError as e:
            print('heartbeat: interrupted')
            if e.errno == zmq.ETERM:
                ctx.term()
                break           # Interrupted


def listener(subport):
    # Set up subscriber to all messages
    ctx = zmq.Context()
    subscriber = ctx.socket(zmq.SUB)
    # # Since this subscriber is just here to make sure we get all messages,
    # # we set CONFLATE to 1, to not use up memory
    subscriber.setsockopt(zmq.LINGER, 0)
    subscriber.setsockopt_string(zmq.SUBSCRIBE, '')
    subscriber.connect(f"tcp://localhost:{subport}")


    # Print everything that arrives on pipe
    while True:
        try:
            msg = subscriber.recv_multipart()
            print(f"{dt.datetime.now().isoformat()} - {msg}")
        except zmq.ZMQError as e:
            print('listener: interrupted')
            if e.errno == zmq.ETERM:
                ctx.term()
                break           # Interrupted


# main thread
# The main task starts the subscriber and publisher, and then sets
# itself up as a listening proxy. The listener runs as a child thread:

def forwarder(pullport=7002, pubport=7001, log_messages=False):
    try:
        context = zmq.Context(1)
        pull_socket = context.socket(zmq.PULL)
        pull_socket.setsockopt(zmq.LINGER, 0)
        pull_socket.set_hwm(1000)
        pull_socket.bind(f"tcp://*:{pullport}")
        
        pub_socket = context.socket(zmq.PUB)
        pub_socket.setsockopt(zmq.LINGER, 0)
        pub_socket.set_hwm(1000)
        pub_socket.bind(f"tcp://*:{pubport}")
        zmq.proxy(pull_socket, pub_socket)
    except Exception as e:
        print("Closing down Forwarder for Data because {}\n".format(e))
       # Create a proxy for the subscriber and publisher
        context.term()

def main (pubport:int = 7001,
          pullport:int = 7002, 
          log_messages:bool = True):

    # Start child threads
    try:
        data_thread = Thread(target=forwarder, args=(pullport,pubport,log_messages))
        typer.echo(f"Starting proxy. Push to : {pullport}, sub from :{pubport}")
        data_thread.start()

        hb_thread = Thread(target=heartbeat, args=(pullport,))
        hb_thread.start()

        if log_messages:
            log_thread = Thread(target=listener, args=(pubport,))
            typer.echo(f"Starting logger")
            log_thread.start()


    except KeyboardInterrupt:
        typer.echo ("Interrupted")
        data_thread.join()
        if log_messages:
            log_thread.join()

if __name__ == '__main__':
    typer.run(main)
import typer
from threading import Thread
import zmq
import datetime as dt
import time
# listener thread
# The listener receives all messages flowing through the proxy, on its
# pipe. Here, the pipe is a pair of ZMQ_PAIR sockets that connects
# attached child threads via inproc. In other languages your mileage may vary:

def logger():
    """ this is the run method of the PAIR thread that logs the messages
    going through the broker """
    ctx = zmq.Context(1)
    sock = ctx.socket(zmq.PAIR)
    sock.connect("tcp://localhost:7328") # connect to the caller
    sock.send(b"") # signal the caller that we are ready
    print("logger: ready")
    while True:
        try:
            msg = sock.recv_multipart()
            print(f"{dt.datetime.now().isoformat()} - {msg}")
        except zmq.ZMQError as e:
            print('logger: interrupted')
            if e.errno == zmq.ETERM:
                ctx.term()
                break

    print("logger: interrupted")  
  

def heartbeat(pushport):
    # Set up subscriber to all messages
    ctx = zmq.Context(1)
    heart_socket = ctx.socket(zmq.PUSH)
    # # Since this heart_socket is just here to make sure we get all messages,
    # # we set CONFLATE to 1, to not use up memory
    heart_socket.setsockopt(zmq.LINGER, 0)
    heart_socket.connect(f"tcp://localhost:{pushport}")


    # Print everything that arrives on pipe
    time.sleep(1)
    while True:
        try:
            heart_socket.send(b'heartbeat')
            time.sleep(60)
        except zmq.ZMQError as e:
            print('heartbeat: interrupted')
            if e.errno == zmq.ETERM:
                ctx.term()
                break           # Interrupted


# main thread
# The main task starts the subscriber and publisher, and then sets
# itself up as a listening proxy. The listener runs as a child thread:

def forwarder(addr="172.24.156.205", pullport=8002, pushport=7001, log_messages=False):
    try:
        context = zmq.Context(1)
        pull_socket = context.socket(zmq.PULL)
        pull_socket.setsockopt(zmq.LINGER, 0)
        pull_socket.set_hwm(1000)
        pull_socket.bind(f"tcp://*:{pullport}")
        
        push_socket = context.socket(zmq.PUSH)
        push_socket.setsockopt(zmq.LINGER, 0)
        push_socket.set_hwm(1000)
        push_socket.connect(f"tcp://{addr}:{pushport}")
        if log_messages:
            log_socket = context.socket(zmq.PAIR)
            log_socket.bind("tcp://*:7328") # use same port as in logger
            zmq.proxy(pull_socket, push_socket, log_socket)
        else:
            zmq.proxy(pull_socket, push_socket)

        typer.echo(f"Forwarding to {addr}:{pushport}")

    except Exception as e:
        print("Closing down Forwarder for Data because {}\n".format(e))
       # Create a proxy for the subscriber and publisher
        context.term()

def main (addr: str = "172.24.156.205",
          pushport:int = 7002,
          pullport:int = 8002, 
          log_messages:bool = True):

    # Start child threads
    try:
        data_thread = Thread(target=forwarder, args=(addr, pullport,pushport,log_messages))
        typer.echo(f"Starting proxy. Push to : {pullport},  will forward to :{addr}:{pushport}")
        data_thread.start()

        hb_thread = Thread(target=heartbeat, args=(pullport,))
        hb_thread.start()

        if log_messages:
            log_thread = Thread(target=logger)
            typer.echo(f"Starting logger")
            log_thread.start()


    except KeyboardInterrupt:
        typer.echo ("Interrupted")
        data_thread.join()
        hb_thread.join()
        if log_messages:
            log_thread.join()

if __name__ == '__main__':
    typer.run(main)

import zmq
import typer
import datetime as dt
import random
import time
import json

def main(addr:str = 'localhost',port: int = 7002):

    ctx = zmq.Context().instance()

    pub = ctx.socket(zmq.PUSH)
    pub.connect(f"tcp://{addr}:{port}")
    name = random.randint(0, 9999)
    i = 0
    pub.send_string(f"Hi. I'm publisher {name}. i have just connected to {addr}:{port}")
    while True:
        d = {"ts": dt.datetime.now().isoformat(), "count": i, "name": name}
        pub.send_string(f"hammer {json.dumps(d)}")
        time.sleep(1.5 + random.random())
        i+=1

if __name__ == '__main__':
    typer.run(main)

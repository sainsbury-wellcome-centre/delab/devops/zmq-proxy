import zmq
import typer
import datetime as dt
import random
def main(addr:str = 'localhost',port: int = 6002):

    ctx = zmq.Context().instance()

    pub = ctx.socket(zmq.PUB)
    pub.setsockopt(zmq.HEARTBEAT_IVL, 60000)
    pub.connect(f"tcp://{addr}:{port}")
    name = random.randint(0, 9999)
    i = 0
    pub.send_string(f"Hi. I'm publisher {name}. i have just connected to {addr}:{port}")
    while True:
        msg = input("Enter message: ")
        d = {"ts": dt.datetime.now().isoformat(), "msg": msg, "count": i, "name": name}
        pub.send_string(f"{msg} {d}")
        i += 1

if __name__ == '__main__':
    typer.run(main)
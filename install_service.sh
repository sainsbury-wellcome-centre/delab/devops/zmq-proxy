#!/bin/bash

USER=$(whoami)

loginctl enable-linger $USER
mkdir -p /home/$USER/.config/systemd/user
ln -s $(pwd)/zmqproxy.service /home/$USER/.config/systemd/user/
systemctl --user daemon-reload
systemctl --user enable zmqproxy.service
systemctl --user start zmqproxy.service

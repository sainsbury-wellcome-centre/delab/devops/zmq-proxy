#!/bin/bash
PATH=~/miniconda3/bin
eval "$(command conda 'shell.bash' 'hook' 2> /dev/null)"

conda activate zmq-proxy
python ~/repos/zmq-proxy/src/zmqproxy.py
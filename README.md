# zmq-proxy

This tiny project will contain code for running a zmq-proxy with option to log all messages.

## Installation

1. [Install miniconda](https://docs.conda.io/en/latest/miniconda.html)
2. `mkdir -p ~/repos/; cd ~/repos`
3. Clone this project: `git clone https://gitlab.com/sainsbury-wellcome-centre/delab/devops/zmq-proxy.git` 
4. install mambda: `conda install -c conda-forge -n base mamba` 
5. change to repo dir: `cd zmq-proxy`
6. install requirements: `mamba env create -f env.yml`
7. To simply run: `./restart_proxy.sh`
8. To install as systemd service `./install_service.sh` 

Note: You will likely need to run `setenforce 0` to disable selinux (or do some selinux setup) on RHEL distros or something related to `apparmor`
on ubunutu distros.

Note: installation of `mamba` is not strictly required. You can run `conda env create -f env.yml` if you don't want to install mamba.

## ToDo:

+ Add conda checks and installation to the shell script
+ Configure this using ansible or puppet?